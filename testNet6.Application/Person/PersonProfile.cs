using testNet6.Application.Person.Queries;
using AutoMapper;

namespace testNet6.Application.Person
{

    public class PersonProfile : Profile
    {
        public PersonProfile()
        {
            CreateMap<testNet6.Domain.Entities.Person, PersonDto>().ReverseMap();
        }
    }
}