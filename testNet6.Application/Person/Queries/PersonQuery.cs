using System.ComponentModel.DataAnnotations;
using MediatR;

namespace testNet6.Application.Person.Queries {
    public record PersonQuery([Required] Guid Id) : IRequest<PersonDto>;
    
}