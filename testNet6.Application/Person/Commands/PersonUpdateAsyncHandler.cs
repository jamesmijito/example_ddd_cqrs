using testNet6.Domain.Services;
using MediatR;

namespace testNet6.Application.Person.Commands {

    public class PersonUpdateAsyncHandler : AsyncRequestHandler<PersonUpdateAsyncCommand>
    {
        public PersonUpdateAsyncHandler()
        {
        }

        protected override async Task Handle(PersonUpdateAsyncCommand request, CancellationToken cancellationToken)
        {
            _ = request ?? throw new ArgumentNullException(nameof(request), "request object needed to handle this task");
        }
    }
}